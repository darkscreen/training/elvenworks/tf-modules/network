//Criar VPC
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"
  enable_dns_hostnames = true

  tags = {
    Name = var.vpc_name,
    terraformed = "true"
  }
}

//Criar Subnets
resource "aws_subnet" "publica1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.cidr_publica1
  availability_zone = "us-east-1a"

  tags = {
    Name = var.nome_publica1
  }
  depends_on = [
    aws_vpc.main
  ]
}
resource "aws_subnet" "publica2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.cidr_publica2
  availability_zone = "us-east-1b"

  tags = {
    Name = var.nome_publica2
  }
  depends_on = [
    aws_vpc.main
  ]
}
resource "aws_subnet" "privada1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.cidr_privada1
  availability_zone = "us-east-1a"

  tags = {
    Name = var.nome_privada1
  }
  depends_on = [
    aws_vpc.main
  ]
}
resource "aws_subnet" "privada2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.cidr_privada2
  availability_zone = "us-east-1b"

  tags = {
    Name = var.nome_privada2
  }
  depends_on = [
    aws_vpc.main
  ]
}

//Cria Internet Gateway
resource "aws_internet_gateway" "internet-gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "iac-internet-gw",
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main
  ]
}

//Cria os IPs dos Nat Gateways
resource "aws_eip" "ip-nat-gateway-1" {
  vpc      = true
  tags = {
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw
  ]
}

resource "aws_eip" "ip-nat-gateway-2" {
  vpc      = true
  tags = {
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw
  ]
}

//Cria os Nat Gateways
resource "aws_nat_gateway" "nat-gateway-1" {
  allocation_id = aws_eip.ip-nat-gateway-1.id
  subnet_id     = aws_subnet.publica1.id

  tags = {
    Name = "iac-nat-gw-1",
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw,
    aws_eip.ip-nat-gateway-1
  ]
}

resource "aws_nat_gateway" "nat-gateway-2" {
  allocation_id = aws_eip.ip-nat-gateway-2.id
  subnet_id     = aws_subnet.publica2.id

  tags = {
    Name = "iac-nat-gw-1",
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw,
    aws_eip.ip-nat-gateway-2
  ]
}

//Cria Tabelas de Roteamento
resource "aws_route_table" "publica" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gw.id
  }
  tags = {
    Name = "iac-rtb-publica",
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw
  ]
}

resource "aws_route_table" "privada1" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gateway-1.id
  }
  tags = {
    Name = "iac-rtb-privada1",
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw
  ]
}
resource "aws_route_table" "privada2" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gateway-2.id
  }
  tags = {
    Name = "iac-rtb-privada2",
    terraformed = "true"
  }
  depends_on = [
    aws_vpc.main,
    aws_internet_gateway.internet-gw
  ]
}

resource "aws_route_table_association" "publica1" {
  subnet_id      = aws_subnet.publica1.id
  route_table_id = aws_route_table.publica.id
}
resource "aws_route_table_association" "publica2" {
  subnet_id      = aws_subnet.publica2.id
  route_table_id = aws_route_table.publica.id
}
resource "aws_route_table_association" "privada1" {
  subnet_id      = aws_subnet.privada1.id
  route_table_id = aws_route_table.privada1.id
}
resource "aws_route_table_association" "privada2" {
  subnet_id      = aws_subnet.privada2.id
  route_table_id = aws_route_table.privada2.id
}

output "vpc_id" {
  value = aws_vpc.main.id
}

output "vpc_cidr" {
  value = aws_vpc.main.cidr_block
}

output "subnet_pub1_id" {
  value = aws_subnet.publica1.id
}

output "subnet_pub1_az" {
  value = aws_subnet.publica1.availability_zone
}

//Cria o Application Load Balancer
resource "aws_security_group" "alb-sg" {
  name        = var.alb_sg_name
  description = "Allow all traffic from VPC CIDR"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "Allow all from VPC CIDR"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [aws_vpc.main.cidr_block]
  }
  ingress {
    description      = "Allow OpenWorld HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.alb_sg_name
    terraformed = "true"
  }
}


resource "aws_lb" "wp-alb" {
  name               = var.wp_alb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb-sg.id]
  subnets            = [
    aws_subnet.publica1.id,
    aws_subnet.publica2.id
  ]
  enable_deletion_protection = false
  tags = {
    terraformed = "true"
  }
}