output "alb_arn" {
  value = aws_lb.wp-alb.arn
}
output "subnet_publica1" {
  value = aws_subnet.publica1.id
}

output "subnet_publica2" {
  value = aws_subnet.publica2.id
}