## network

This module create a network at AWS with this resources:

1 - VPC<br>
2 - Public Subnets<br>
2 - Private Subnets<br>
2 - Elastic Ips for Nat Gateways<br>
1 - Internet Gateway<br>
2 - Nat Gateways<br>
1 - Public Route<br>
2 - Private Routes<br>
And the associations between public and private subnets with their routes.<br>